# Projecte final sobre Suricata

# Model de Xarxa

## TCP / IP

![imagen tcp/ip](aux/tcp_ip.jpg)

# Tallafocs

### Què és un tallafocs, o firewall?

### Com s'implanten dins d'una xarxa?

![imagen](aux/firewall.png)

# Iptables

### Què són les iptables?

## Iptables i Suricata

#### Com podem implementar tots dos programes?

# IDS / IPS

## IPS

### Què volen dir les sigles IPS i IDS?

![imagen](aux/ips_ids.png)

### En la següent imatge es pot observar clarament la diferencia entre **IPS** i **IDS**

![imagen](aux/dif.jpg)

# Suricata

Suricata és un software que pot actuar com un sistema IDS o bé com a IPS.

És multithread, a diferència de *Snort*, que és l'altre programa open source d'IDS/IPS més utilitzat.

La versió més actual de Suricata és la 4.0, algunes de les millores respecte a les versions anteriors són:

1. Afegides regles per a controlar els protocols: *http, ssh*
2. Fitxer de logs en format json i EVE

Les caracteristiques més destacables són:

- Processament multifil
- Captura de paquets
- Decodificador
- Detecció i comprovació amb les reglas
- Procesament de sortida de alertas
- Detecció automatica dels protocols
- Analisis del rendiment

![imagen](aux/suricata.jpg)

# Comandes de Suricata

### Per a què serveixen?

## Paràmetres

Alguns dels més rellevants són:

+ -h
+ -V
+ -c <ruta>
+ -s <fitxer_regles>
+ -S <fitxer_regles>
+ -i <interficie>

# Regles

### Què són les regles?

### Com estàn formades?

- Acció
- Protocol
- Xarxa
- Ports
- Direcció (Entre la xarxa local i la xarxa externa)
- Paràmetres

# Docker

### Què és Docker?

![imagen](aux/docker.png)

### Per a que els utilitzarem?

Les imatges de dockers que utilitzarem són:

1. **isx46420653/projecte:ftp**

2. **isx46420653/projecte:http**

3. **edtasixm11/net18**

# Amazon

### Què és Amazon AWS i perquè s'utilitza?


Algunes de les eines de les quals disposa Amazon són:

- Cloud Computing
- Bases de Dades
- Creació de xarxes virtuals
- Emmagatzament i gestors de contingut
- Seguretat i Control d'accès

### Per a què l'utilitzem nosaltres?

![imagen](aux/aws.png)

# Exemple pràctic Suricata

Cas 1


