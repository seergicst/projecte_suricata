#/bin/bash

# Copiem els fitxers de configuració
cp httpd.conf /etc/httpd/conf/httpd.conf
cp /opt/docker/calculadora.py /var/www/cgi-bin/
chmod +x /var/www/cgi-bin/calculadora.py

# Pàgina web
cp index.html /var/www/index.html
cp calculadora.css /var/www/

# Canviem tot al propietari apache
chown -R apache.apache /var/www/
