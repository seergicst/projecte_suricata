**prova:**

Ssh entre maquines d'Amazon

**ordre realitzada:**

```
[root@ip-172-31-24-223 ~]# ssh fedora@172.31.18.5
```

**logs:**

```
05/15/2020-11:32:58.998851  [Drop] [**] [1:201:1] prohibit acces ssh a tothom excepte hosts verificats [**] [Classification: (null)] [Priority: 3] {TCP} 172.31.24.223:57502 -> 172.31.18.5:22
05/15/2020-11:32:58.998851  [**] [1:101:1] intent de connexió ssh local cap al noste servidor [**] [Classification: (null)] [Priority: 3] {TCP} 172.31.24.223:57502 -> 172.31.18.5:22
```

**prova:**

Petició a http extern

**ordre realitzada:**

```
[root@ip-172-31-18-5 rules]# wget www.google.es
```

**logs:**

```
01/01/1970-01:30:24.254265  [**] [1:102:1] el host local ha accedit a un servidor http extern [**] [Classification: (null)] [Priority: 3] {TCP} 172.31.18.5:50028 -> 216.58.204.67:80
01/01/1970-01:30:24.256677  [**] [1:102:1] el host local ha accedit a un servidor http extern [**] [Classification: (null)] [Priority: 3] {TCP} 172.31.18.5:50028 -> 216.58.204.67:80
```

**prova:**

Alerta de pings entre xarxes locals

**ordre realitzada:**

```
[root@ip-172-31-24-223 tmp]# ping 172.31.18.5
```

**logs:**

```
05/15/2020-11:42:22.577550  [**] [1:103:1] intent de comprovació de connexions entre hosts locals [**] [Classification: (null)] [Priority: 3] {ICMP} 172.31.24.223:8 -> 172.31.18.5:0
05/15/2020-11:42:22.578841  [**] [1:103:1] intent de comprovació de connexions entre hosts locals [**] [Classification: (null)] [Priority: 3] {ICMP} 172.31.18.5:0 -> 172.31.24.223:0
```

**prova:**

Denegar el ping d'una maquina determinada a xarxes externes

**ordre realitzada:**

```
[root@ip-172-31-18-5 rules]# ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
From 8.8.8.8 icmp_seq=1 Destination Host Prohibited
```

**logs:**

```
05/15/2020-11:44:37.768184  [Drop] [**] [1:104:1] prohibit els ping a l'exterior al host local concret [**] [Classification: (null)] [Priority: 3] {ICMP} 172.31.18.5:8 -> 8.8.8.8:0
```

**prova:**

Host local es connecta a un ftp

**ordre realitzada:**

```
[root@ip-172-31-24-223 tmp]# ftp 172.31.18.5 21
Connected to 172.31.18.5 (172.31.18.5).
220 (vsFTPd 3.0.3)
Name (172.31.18.5:root): anonymous
```

**logs:**

```
05/15/2020-11:51:43.413513  [**] [1:105:1] host local accedint a un ftp [**] [Classification: (null)] [Priority: 3] {TCP} 172.18.0.4:21 -> 172.31.24.223:45342
05/15/2020-11:51:43.413965  [**] [1:105:1] host local accedint a un ftp [**] [Classification: (null)] [Priority: 3] {TCP} 172.31.24.223:45342 -> 172.18.0.4:21
```

**prova:**

Host local no es connecta a http amb port !80

**ordre realitzada:**

```
[fedora@ip-172-31-18-5 ~]$ wget 35.178.239.18:8000
```

**logs:**

```
01/01/1970-00:18:37.689660  [Drop] [**] [1:106:1] no es permet la connexió a http que no corresponguin al port 80 [**] [Classification: (null)] [Priority: 3] {TCP} 172.31.18.5:35370 -> 35.178.239.18:8000
```

**prova:**

Les xarxes locals no poden utilitzar el daytimeserver del port 13

**ordre realitzada:**

```
[root@ip-172-31-24-223 tmp]# telnet 172.31.18.5 13
Trying 172.31.18.5...
```

**logs:**

```
05/15/2020-11:56:43.749277  [Drop] [**] [1:108:1] xarxes locals no tenen acces al port 13 [**] [Classification: (null)] [Priority: 3] {TCP} 172.31.24.223:57120 -> 172.18.0.2:13
```

```
[Pau@portatil tmp]$ telnet 3.8.237.24 13
Trying 3.8.237.24...
Connected to 3.8.237.24.
```

Des de un host extern si deixa la connexió

**prova:**

Permetre ssh només de Ips especificàs

**ordre realitzada:**

```
[root@ip-172-31-28-253 ~]# ssh -i clau.pem fedora@52.56.148.209
```

**logs:**

```
05/16/2020-15:12:22.246990  [Drop] [**] [1:201:1] prohibit acces ssh a tothom excepte hosts verificats [**] [Classification: (null)] [Priority: 3] {TCP} 35.178.239.18:37858 -> 172.31.18.5:22
```

```
[Pau@portatil ~]$ ssh -i clau.pem fedora@52.56.148.209
[fedora@ip-172-31-18-5 ~]$
```

Des de un host especific si que esta permès el ssh

Per entendre millor el cas es pot observar el següent diagrama.

![imagen](./diagrama.png)

**prova:**

Provar els pings des de l'exterior

**ordre realitzada:**

```
[Pau@portatil tmp]$ ping 3.8.237.24
```

**logs:**

```
05/15/2020-12:00:49.756337  [**] [1:202:1] ICMP detectat provenint de l'exterior [**] [Classification: (null)] [Priority: 3] {ICMP} 93.176.144.114:3 -> 172.31.18.5:10
05/15/2020-12:00:49.756413  [**] [1:202:1] ICMP detectat provenint de l'exterior [**] [Classification: (null)] [Priority: 3] {ICMP} 93.176.144.114:3 -> 172.31.18.5:10
```

**prova:**

Denegar accés http port 80

**ordre realitzada:**

```
[Pau@portatil tmp]$ wget 3.8.237.24
--2020-05-15 14:01:55--  http://3.8.237.24/
Conectando con 3.8.237.24:80... conectado.
Petición HTTP enviada, esperando respuesta...
```

**logs:**

```
05/15/2020-12:02:46.503289  [Drop] [**] [1:203:1] denegat acces al servidor http des de  xarxes externes [**] [Classification: (null)] [Priority: 3] {TCP} 93.176.144.114:12517 -> 172.31.18.5:80
```

**prova:**

Denegar accés ftp a qualsevol host extern

**ordre realitzada:**

```
[Pau@portatil tmp]$ ftp 3.8.237.24
Connected to 3.8.237.24 (3.8.237.24).
220 (vsFTPd 3.0.3)
Name (3.8.237.24:Pau): anonymous
```

**logs:**

```
05/15/2020-12:04:09.985562  [**] [1:105:1] host local accedint a un ftp [**] [Classification: (null)] [Priority: 3] {TCP} 172.18.0.4:21 -> 93.176.144.114:12563
05/15/2020-12:04:10.124802  [Drop] [**] [1:204:1] denegat acces al servidor ftp des de xarxes externes [**] [Classification: (null)] [Priority: 3] {TCP} 93.176.144.114:12563 -> 172.18.0.4:21
```

**prova:**

Denegar ús del echo-server al port 7

**ordre realitzada:**

```
[Pau@portatil tmp]$ telnet 3.8.237.24 7
Trying 3.8.237.24...
```

**logs:**

```
05/15/2020-12:12:12.233747  [Drop] [**] [1:206:1] denegat acces port 7 host extern [**] [Classification: (null)] [Priority: 3] {TCP} 93.176.144.114:12874 -> 172.18.0.2:7
05/15/2020-12:12:13.280300  [Drop] [**] [1:206:1] denegat acces port 7 host extern [**] [Classification: (null)] [Priority: 3] {TCP} 93.176.144.114:12874 -> 172.18.0.2:7
```
