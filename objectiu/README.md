# Objectiu projecte Suricata

Per a poder utilitzar i comprovar el software de Suricata, el que farem serà muntar un host el qual farà de servidor, on hi hauran certs serveis que ens facilitaran la feina de configuració del Suricata.

Un cop creat aquest servidor configurarem Suricata perque controli tant l'accés de connexions que provenen del exterior com aquelles que vinguin de hosts de la xarxa local.

Al tenir les configuracions ben creades, podrem comprovar-ho desde els logs o intentant fer connexions desde els hosts, la funció que realitzarà Suricata.

![imagen](./diagrama.png)
