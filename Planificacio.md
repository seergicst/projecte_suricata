# Planificació

### Guió que seguirem a la hora de fer el nostre projecte final de CFGS

1. Models de Xarxa

        Explicarem el model actual (TCP / IP)

2. Eines seguretat en xarxa (iptables, vpn)

        Explicarem que son tan les iptables con les vpn

3. Amazon AWS

        Breu resum sobre que es el AWS i la estructura principal que tindrem

4. Muntatge del servidor ldap a una instància d’Amazon
5. Muntatge del servidor Kerberos a una instància d’Amazon
6. Muntatge del servidor ftp a una instància d’Amazon
7. Muntatge del servidor phpldapadmin a una instància d’Amazon
8. Regles iptables

        Configuració sobre els input i output que tindran els nostres servidors

9. Túnels VPN

        Creació de vpn per a poder tenir accés a tots els servidors desde
        l'exterior de la xarxa local

10. Suricata. Què és?
11. Com funciona?
12. Configuració del Suricata
13. Exemple: Entrar a la xarxa des d’una IP externa
14. Exposició dels logs
15. Conclusió

Tenim dos possibles estructures a muntar per a la demostració del funcionament del
Suricata.

- Creació dels servidors a diverses maquines AWS

- Creació dels servidors en maquines locals i us d' AWS com a router


### Temporització

#### Primera setmana:

Muntatges dels servidors i estructures a seguir.  
Comprovar el funcionament d'aquests

#### Segona setmana:

Creació de les vpn necessàries i la configuració de les regles iptables

#### Tercera setmana:

Configuració del Suricata i comprovació de logs.  
Teoria de xarxes

#### Quarta setmana:

Conclusió i cartell.  
Fer presentació
