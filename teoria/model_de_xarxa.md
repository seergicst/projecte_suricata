# MODEL TCP / IP

## Protocol de control de transmissió / protocol de Internet

El model TCP / IP, compren un seguit de protocols distribuïts en diferents capes invisibles o nivells, la unió de tots aquests protocols, possibilita que es puguin enviar missatges i senyals entre diferents xarxes.

Els missatges surten del nivell d'aplicació d'un host, passant a la capa de transport, viatjant per Internet a traves de cables i routers per accedir a la xarxa destí y després tornar a pujar a la capa de Internet, transport i aplicació.

Les dades que s’envien van encapsulades i cada nivell proporciona una capçalera diferent que executa unes funcions i proporciona serveis al nivell superior.

![imagen tcp/ip](../aux/tcp_ip.jpg)
