# Comandes de Suricata

Les comandes de Suricata serveixen per indicar amb quines funcions volem iniciar el programa, també podem indicar fitxers de configuració o mode de funcionament diferents als de per defecte amb alguns paràmetres específics

## Paràmetres

+ -h

Mostra l'ajuda que incorpora el programa

+ -V

Mostra la versió del programa

+ -c <ruta>

És l'opció més important, indica la ruta al fitxer de configuració de Suricata

+ -T

Realitza un test per a comprovar que la configuració és correcta i no dóna error, no vol dir que les regles funcionin correctament, només que els fitxers de configuració si que ho fan.

+ -v: Info

Incrementa el nivell de log de manera que mostra més informació del funcionament del programa, podem especificar de què volem obtenir més informació amb les següents opcions:

+ -vv: Performance
+ -vvv: Configuració
+ -vvvv: Debug

Aquestes opcions no poden disminuir el fluxe d'informació que és dóna al fitxer de logs, només ho poden incrementar.

+ -s <fitxer_regles>

Fitxer de regles a executar, s'afegeix als fitxers de regles definits al fitxer de configuració yaml.

+ -S <fitxer_regles>

Fitxer de regles a executar, s'executen les regles exclusivament d'aquest fitxer. 

+ -i <interficie>

Indica l'interficie de xarxa a utilitzar, podem indicar més d'una

+ -l <directory>

Indica per defecte el directori dels logs

+ -D

Executa Suricata en background

+ -q

Executar en el mode NFQ

+ --list-runmodes

Llista tots els modes d'execució del programa

+ --runmode <runmode>

Indica el mode d'execució amb el que vols que s'executi Suricata, hi han 3 de diferents:
1. Workers
2. autofp
3. single

[Documentació](https://suricata.readthedocs.io/en/suricata-5.0.3/performance/runmodes.html) sobre els runmodes

+ --user=<user>

Indica l'usuari amb el que vols executar Suricata.

+ --group=<group>

Indica el grup amb el que vols executar Suricata.

+ --pidfile <file>

Guarda l'identificador de procés del programa al fitxer indicat.

+ --list-app-layer-protos

Llista tots els protocols de xarxa que accepta Suricata.

+ --list-keywords=[all|csv|<kword>]

Llista els paràmetres opcionals que es poden afegir a les regles de Suricata, ho pot llistar en format csv o bé buscar només aquell keyword específic.
