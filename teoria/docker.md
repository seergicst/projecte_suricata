# Docker

## Què és Docker?

Docker és un projecte de codi obert que automatitza el desplegament d'aplicacions
dins de contenidors de programari, proporcionant una capa addicional d'abstracció
i automatització de virtualització d'aplicacions en múltiples sistemes operatius

![imagen](../aux/docker.png)

## Per a que els utilitzarem?

En el nostre projecte, utilitzarem els Dockers, per a crear de forma virtual servidors,
els quals ens ajudaran a poder configurar i entre de millor manera Suricata.

Utilitzarem 3 dockers, un per a representar un servidor http, un altre per a
representar un servidor ftp i l'últim que ens redireccionar els ports 7, 13 i 19
per a poder tenir més opcions en la configuració del suricata.

Els dockers es podran trobar a:

**isx46420653/projecte:ftp**

**isx46420653/projecte:http**

**edtasixm11/net18**
